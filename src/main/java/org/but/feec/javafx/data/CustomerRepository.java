package org.but.feec.javafx.data;

import org.but.feec.javafx.api.*;
import org.but.feec.javafx.config.DataSourceConfig;
import org.but.feec.javafx.exceptions.DataAccessException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerRepository {

    public CustomerAuthView findCustomerByFirstName(String first_name) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT first_name, password" +
                             " FROM public.customer c" +
                             " WHERE c.first_name = ?")
        ) {
            preparedStatement.setString(1, first_name);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToCustomerAuth(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find person by ID with addresses failed.", e);
        }
        return null;
    }

    public CustomerDetailView findCustomerDetailedView(Long customerId) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT c.customer_id, email, first_name, last_name, city, street_name, street_number, address_type" +
                             " FROM public.customer c" +
                             " JOIN cust_address ca ON c.customer_id = ca.customer_id" +
                             " JOIN address a ON a.address_id = ca.address_id" +
                             " WHERE c.customer_id = ?")
        ) {
            preparedStatement.setLong(1, customerId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToCustomerDetailView(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find customer by ID with addresses failed.", e);
        }
        return null;
    }


    public List<CustomerBasicView> getCustomersBasicView() {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT c.customer_id, email, first_name, last_name, city" +
                             " FROM public.customer c" +
                             " LEFT JOIN cust_address ca ON c.customer_id = ca.customer_id" +
                             " LEFT JOIN address a ON a.address_id=ca.address_id");
             ResultSet resultSet = preparedStatement.executeQuery();) {
            List<CustomerBasicView> customerBasicViews = new ArrayList<>();
            while (resultSet.next()) {
                customerBasicViews.add(mapToCustomerBasicView(resultSet));
            }
            return customerBasicViews;
        } catch (SQLException e) {
            throw new DataAccessException("Persons basic view could not be loaded.", e);
        }
    }
    public List<SqlInjectView> getSqlInjectView(String input) {
        String query = "SELECT rp.people_id, first_name, last_name, age, puppy FROM inject.rnd_people rp WHERE rp.people_id ="+input;
        try (Connection connection = DataSourceConfig.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query);) {
            List<SqlInjectView> injectViews = new ArrayList<>();
            while (resultSet.next()) {
                injectViews.add(mapToSqlInjectView(resultSet));
            }
            return injectViews;
        } catch (SQLException e) {
            throw new DataAccessException("People sql injection could not be loaded.", e);
        }
    }

    public List<CustomerFilterView> getCustomerFilterView(String text) {
        String filter = '%'+text+'%';
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT c.customer_id, email, first_name, last_name, city" +
                             " FROM public.customer c" +
                             " LEFT JOIN cust_address ca ON c.customer_id = ca.customer_id" +
                             " LEFT JOIN address a ON a.address_id=ca.address_id" +
                             " WHERE c.first_name LIKE  ?");

             ) {
            preparedStatement.setString(1,filter);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<CustomerFilterView> filterView = new ArrayList<>();
            while (resultSet.next()) {
                filterView.add(mapToCustomerFilterView(resultSet));
            }
            return filterView;
        } catch (SQLException e) {
            throw new DataAccessException("Filter view could not be loaded", e);
        }
    }

    public void createCustomer(CustomerCreateView customerCreateView) {
        String insertPersonSQL = "INSERT INTO public.customer (email, first_name, last_name, password, gender) VALUES (?,?,?,?,?)";
        try (Connection connection = DataSourceConfig.getConnection();

             PreparedStatement preparedStatement = connection.prepareStatement(insertPersonSQL, Statement.RETURN_GENERATED_KEYS)) {


            preparedStatement.setString(1, customerCreateView.getEmail());
            preparedStatement.setString(2, customerCreateView.getFirstName());
            preparedStatement.setString(3, customerCreateView.getLastName());
            preparedStatement.setString(4, String.valueOf(customerCreateView.getPwd()));
            preparedStatement.setString(5, String.valueOf(customerCreateView.getGender()));


            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new DataAccessException("Creating customer failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating customer failed operation on the database failed.");
        }
    }

    public static void deleteCustomer(Long id){
        String deleteCustomerSQL = "DELETE FROM public.customer c WHERE c.customer_id = ? ";

        try (Connection connection = DataSourceConfig.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(deleteCustomerSQL, Statement.RETURN_GENERATED_KEYS)){

            preparedStatement.setLong(1,id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DataAccessException("Deleting customer failed operation on the database failed.");
        }
    }



    public void editCustomer(CustomerEditView customerEditView) {
        String insertCustomerSQL = "UPDATE public.customer c SET email = ?, first_name = ?,  last_name = ? WHERE c.customer_id = ?";
        String checkIfExists = "SELECT first_name FROM public.customer c WHERE c.customer_id = ?";
        try (Connection connection = DataSourceConfig.getConnection();

             PreparedStatement preparedStatement = connection.prepareStatement(insertCustomerSQL, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, customerEditView.getEmail());
            preparedStatement.setString(2, customerEditView.getFirstName());
            preparedStatement.setString(3, customerEditView.getLastName());
            preparedStatement.setLong(4, customerEditView.getId());

            try {

                connection.setAutoCommit(false);

                try (PreparedStatement ps = connection.prepareStatement(checkIfExists, Statement.RETURN_GENERATED_KEYS)) {
                    ps.setLong(1, customerEditView.getId());
                    ps.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("This person for edit do not exists.");
                }

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0) {
                    throw new DataAccessException("Creating person failed, no rows affected.");
                }

                connection.commit();
            } catch (SQLException e) {

                connection.rollback();
            } finally {

                connection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            throw new DataAccessException("Editing customer failed operation on the database failed.");
        }
    }



    private CustomerAuthView mapToCustomerAuth(ResultSet rs) throws SQLException {
        CustomerAuthView customer = new CustomerAuthView();
        customer.setFirst_name(rs.getString("first_name"));
        customer.setPassword(rs.getString("password"));
        return customer;
    }

    private CustomerBasicView mapToCustomerBasicView(ResultSet rs) throws SQLException {
        CustomerBasicView customerBasicView = new CustomerBasicView();
        customerBasicView.setId(rs.getLong("customer_id"));
        customerBasicView.setEmail(rs.getString("email"));
        customerBasicView.setFirstName(rs.getString("first_name"));
        customerBasicView.setLastName(rs.getString("last_name"));
        customerBasicView.setCity(rs.getString("city"));
        return customerBasicView;
    }

    private CustomerFilterView mapToCustomerFilterView(ResultSet rs) throws SQLException {
        CustomerFilterView customerFilterView = new CustomerFilterView();
        customerFilterView.setfId(rs.getLong("customer_id"));
        customerFilterView.setfEmail(rs.getString("email"));
        customerFilterView.setfFirstName(rs.getString("first_name"));
        customerFilterView.setfLastName(rs.getString("last_name"));
        customerFilterView.setfCity(rs.getString("city"));
        return customerFilterView;
    }

    private CustomerDetailView mapToCustomerDetailView(ResultSet rs) throws SQLException {
        CustomerDetailView customerDetailView = new CustomerDetailView();
        customerDetailView.setId(rs.getLong("customer_id"));
        customerDetailView.setEmail(rs.getString("email"));
        customerDetailView.setFirstName(rs.getString("first_name"));
        customerDetailView.setLastName(rs.getString("last_name"));
        customerDetailView.setCity(rs.getString("city"));
        customerDetailView.setStreetName(rs.getString("street_name"));
        customerDetailView.setStreetNumber(rs.getString("street_number"));
        customerDetailView.setAddressType(rs.getString("address_type"));
        return customerDetailView;
    }

    private SqlInjectView mapToSqlInjectView(ResultSet rs ) throws  SQLException{
        SqlInjectView injectionView = new SqlInjectView();
        injectionView.setPeoId(rs.getLong("people_id"));
        injectionView.setPeoFirstName(rs.getString("first_name"));
        injectionView.setPeoLastName(rs.getString("last_name"));
        injectionView.setPeoAge(rs.getLong("age"));
        injectionView.setPeoPuppy(rs.getString("puppy"));
        return injectionView;
    }

}
