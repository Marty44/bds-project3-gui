package org.but.feec.javafx.api;

import java.util.Arrays;
import java.sql.Timestamp;




public class CustomerCreateView {

    private String firstName;
    private String lastName;
    private String email;
    private char[] pwd;
    private char [] gender;


    public char[] getGender() {
        return gender;
    }

    public void setGender(char[] gender) {
        this.gender = gender;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public char[] getPwd() {
        return pwd;
    }

    public void setPwd(char[] pwd) {
        this.pwd = pwd;
    }

    @Override
    public String toString() {
        return "PersonCreateView{" +
                "email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + Arrays.toString(gender) + '\'' +
                ", pwd=" + Arrays.toString(pwd) +
                '}';
    }
}
