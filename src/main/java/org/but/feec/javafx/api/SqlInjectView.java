package org.but.feec.javafx.api;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SqlInjectView {

    private LongProperty peoId = new SimpleLongProperty();
    private StringProperty peoFirstName = new SimpleStringProperty();
    private StringProperty peoLastName = new SimpleStringProperty();
    private LongProperty peoAge = new SimpleLongProperty();
    private StringProperty peoPuppy = new SimpleStringProperty();

    public long getPeoId() {
        return peoId.get();
    }

    public LongProperty peoIdProperty() {
        return peoId;
    }

    public void setPeoId(long peoId) {
        this.peoId.set(peoId);
    }

    public String getPeoFirstName() {
        return peoFirstName.get();
    }

    public StringProperty peoFirstNameProperty() {
        return peoFirstName;
    }

    public void setPeoFirstName(String peoFirstName) {
        this.peoFirstName.set(peoFirstName);
    }

    public String getPeoLastName() {
        return peoLastName.get();
    }

    public StringProperty peoLastNameProperty() {
        return peoLastName;
    }

    public void setPeoLastName(String peoLastName) {
        this.peoLastName.set(peoLastName);
    }

    public Long getPeoAge() {
        return peoAge.get();
    }

    public LongProperty peoAgeProperty() {
        return peoAge;
    }

    public void setPeoAge(Long peoAge) {
        this.peoAge.set(peoAge);
    }

    public String getPeoPuppy() {
        return peoPuppy.get();
    }

    public StringProperty peoPuppyProperty() {
        return peoPuppy;
    }

    public void setPeoPuppy(String peoPuppy) {
        this.peoPuppy.set(peoPuppy);
    }


}
