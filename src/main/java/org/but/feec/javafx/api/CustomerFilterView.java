package org.but.feec.javafx.api;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CustomerFilterView {
    private LongProperty id = new SimpleLongProperty();
    private StringProperty city = new SimpleStringProperty();
    private StringProperty email = new SimpleStringProperty();
    private StringProperty firstName = new SimpleStringProperty();
    private StringProperty lastName = new SimpleStringProperty();

    public long getfId() {
        return id.get();
    }

    public LongProperty idProperty() {
        return id;
    }

    public void setfId(long id) {
        this.id.set(id);
    }

    public String getfCity() {
        return city.get();
    }

    public StringProperty cityProperty() {
        return city;
    }

    public void setfCity(String city) {
        this.city.set(city);
    }

    public String getfEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public void setfEmail(String email) {
        this.email.set(email);
    }

    public String getfFirstName() {
        return firstName.get();
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public void setfFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public String getfLastName() {
        return lastName.get();
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public void setfLastName(String lastName) {
        this.lastName.set(lastName);
    }




}
