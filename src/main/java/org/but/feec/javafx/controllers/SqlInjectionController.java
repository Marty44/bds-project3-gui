package org.but.feec.javafx.controllers;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.but.feec.javafx.api.SqlInjectView;
import org.but.feec.javafx.data.CustomerRepository;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.but.feec.javafx.services.CustomerService;

import java.util.List;
public class SqlInjectionController {
    @FXML
    private TableColumn<SqlInjectView, Long> peoIdColumn;
    @FXML
    private TableColumn<SqlInjectView, String> peoFnColumn;
    @FXML
    private TableColumn<SqlInjectView, String> peoLnColumn;
    @FXML
    private TableColumn<SqlInjectView, Long> peoAgeColumn;
    @FXML
    private TableColumn<SqlInjectView, String> peoPupColumn;
    @FXML
    private TextField injectTextField;
    @FXML
    private Button injectButton;
    @FXML
    private TableView<SqlInjectView> systemPeoTableView;

    private CustomerService customerService;
    private CustomerRepository customerRepository;

    public Stage stage;
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void initialize() {
        customerRepository = new CustomerRepository();
        customerService = new CustomerService(customerRepository);
//        GlyphsDude.setIcon(exitMenuItem, FontAwesomeIcon.CLOSE, "1em");

        peoIdColumn.setCellValueFactory(new PropertyValueFactory<SqlInjectView, Long>("peoId"));
        peoFnColumn.setCellValueFactory(new PropertyValueFactory<SqlInjectView, String>("peoFirstName"));
        peoLnColumn.setCellValueFactory(new PropertyValueFactory<SqlInjectView, String>("peoLastName"));
        peoAgeColumn.setCellValueFactory(new PropertyValueFactory<SqlInjectView, Long>("peoAge"));
        peoPupColumn.setCellValueFactory(new PropertyValueFactory<SqlInjectView, String>("peoPuppy"));

//        ObservableList<SqlInjectView> observablePeoList = initializePeoData();
//        systemPeoTableView.setItems(observablePeoList);
//        loadIcons();


    }
    private ObservableList<SqlInjectView> initializePeoData(){

        String input = injectTextField.getText();
        List<SqlInjectView> people = customerService.getSqlInjectView(input);
        return FXCollections.observableArrayList(people);
    }
    public void handleInjectButton(ActionEvent actionEvent){

        ObservableList<SqlInjectView> observablePeoList = initializePeoData();
        systemPeoTableView.setItems(observablePeoList);
    }


}
