package org.but.feec.javafx.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.but.feec.javafx.App;
import org.but.feec.javafx.api.CustomerBasicView;
import org.but.feec.javafx.api.CustomerDetailView;
import org.but.feec.javafx.data.CustomerRepository;
import org.but.feec.javafx.exceptions.ExceptionHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import org.but.feec.javafx.services.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class CustomersController {

    private static final Logger logger = LoggerFactory.getLogger(CustomersController.class);

    @FXML
    public Button addCustomerButton;
    @FXML
    public Button refreshButton;
    @FXML
    public Button filterButton;
    @FXML
    public Button sqlInjectionButton;
    @FXML
    public TextField filterTextField;

    @FXML
    private TableColumn<CustomerBasicView, Long> customersId;
    @FXML
    private TableColumn<CustomerBasicView, String> customersCity;
    @FXML
    private TableColumn<CustomerBasicView, String> customersEmail;
    @FXML
    private TableColumn<CustomerBasicView, String> customersFirstName;
    @FXML
    private TableColumn<CustomerBasicView, String> customersLastName;
    @FXML
    private TableView<CustomerBasicView> systemCustomersTableView;
//    @FXML
//    public MenuItem exitMenuItem;

    private CustomerService customerService;
    private CustomerRepository customerRepository;

    public CustomersController() {
    }

    @FXML
    private void initialize() {
        customerRepository = new CustomerRepository();
        customerService = new CustomerService(customerRepository);
//        GlyphsDude.setIcon(exitMenuItem, FontAwesomeIcon.CLOSE, "1em");

        customersId.setCellValueFactory(new PropertyValueFactory<CustomerBasicView, Long>("id"));
        customersCity.setCellValueFactory(new PropertyValueFactory<CustomerBasicView, String>("city"));
        customersEmail.setCellValueFactory(new PropertyValueFactory<CustomerBasicView, String>("email"));
        customersFirstName.setCellValueFactory(new PropertyValueFactory<CustomerBasicView, String>("firstName"));
        customersLastName.setCellValueFactory(new PropertyValueFactory<CustomerBasicView, String>("lastName"));


        ObservableList<CustomerBasicView> observablePersonsList = initializeCustomersData();
        systemCustomersTableView.setItems(observablePersonsList);

        systemCustomersTableView.getSortOrder().add(customersId);

        initializeTableViewSelection();
//        loadIcons();

        logger.info("CustomersController initialized");
    }

    private void initializeTableViewSelection() {
        MenuItem edit = new MenuItem("Edit customer");
        MenuItem detailedView = new MenuItem("Detailed customer view");
        MenuItem delete = new MenuItem("Delete customer");
        edit.setOnAction((ActionEvent event) -> {
            CustomerBasicView personView = systemCustomersTableView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/CustomerEdit.fxml"));
                Stage stage = new Stage();
                stage.setUserData(personView);
                stage.setTitle("BDS JavaFX Edit Person");

                CustomerEditController controller = new CustomerEditController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });

        detailedView.setOnAction((ActionEvent event) -> {
            CustomerBasicView customerView = systemCustomersTableView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/CustomerDetailView.fxml"));
                Stage stage = new Stage();

                Long customerId = customerView.getId();
                CustomerDetailView customerDetailView = customerService.getCustomerDetailView(customerId);

                stage.setUserData(customerDetailView);
                stage.setTitle("BDS JavaFX Customers Detailed View");

                CustomerDetailViewController controller = new CustomerDetailViewController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });
        delete.setOnAction((ActionEvent event) -> {
            CustomerBasicView customerView = systemCustomersTableView.getSelectionModel().getSelectedItem();
            try {
                Long customersId = customerView.getId();
                customerRepository.deleteCustomer(customersId);

            } catch(Exception e){
                ExceptionHandler.handleException(e);
                e.printStackTrace();
            }
        });


        ContextMenu menu = new ContextMenu();
        menu.getItems().add(edit);
        menu.getItems().addAll(detailedView);
        menu.getItems().add(delete);
        systemCustomersTableView.setContextMenu(menu);
    }

    private ObservableList<CustomerBasicView> initializeCustomersData() {
        List<CustomerBasicView> customers = customerService.getCustomersBasicView();
        return FXCollections.observableArrayList(customers);
    }

//    private void loadIcons() {
//        Image vutLogoImage = new Image(App.class.getResourceAsStream("logos/vut-logo-eng.png"));
//        ImageView vutLogo = new ImageView(vutLogoImage);
//        vutLogo.setFitWidth(150);
//        vutLogo.setFitHeight(50);
//    }

    public void handleExitMenuItem(ActionEvent event) {
        System.exit(0);
    }

    public void handleAddCustomerButton(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/CustomerCreate.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 500);
            Stage stage = new Stage();
            stage.setTitle("BDS JavaFX Create Person");
            stage.setScene(scene);


            Stage stageOld = (Stage) addCustomerButton.getScene().getWindow();
            stageOld.close();

//            stage.getIcons().add(new Image(App.class.getResourceAsStream("logos/vut.jpg")));
            authConfirmDialog();


            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }
    public void handleSqlInjectionButton(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/SqlInjection.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 500);
            Stage stage = new Stage();
            stage.setTitle("SQL Injection Example");
            stage.setScene(scene);


            Stage stageOld = (Stage) sqlInjectionButton.getScene().getWindow();
            stageOld.close();

//            stage.getIcons().add(new Image(App.class.getResourceAsStream("logos/vut.jpg")));

            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }
    public void handleFilterButton(ActionEvent actionEvent){
        try{
            String text = filterTextField.getText();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/CustomersFilter.fxml"));

            Stage stage = new Stage();
            stage.setUserData(text);
            stage.setTitle("Customer Filter");

            CustomerFilterController customerFilterController = new CustomerFilterController();
            customerFilterController.setStage(stage);
            fxmlLoader.setController(customerFilterController);

            Scene scene = new Scene(fxmlLoader.load(), 600, 500);

            stage.setScene(scene);

            stage.show();

        }catch(IOException e){
            ExceptionHandler.handleException(e);
        }
    }


    private void authConfirmDialog() {
    }

    public void handleRefreshButton(ActionEvent actionEvent) {
        ObservableList<CustomerBasicView> observablePersonsList = initializeCustomersData();
        systemCustomersTableView.setItems(observablePersonsList);
        systemCustomersTableView.refresh();
        systemCustomersTableView.sort();
    }
}
