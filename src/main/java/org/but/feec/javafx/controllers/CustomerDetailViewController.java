package org.but.feec.javafx.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.but.feec.javafx.api.CustomerDetailView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomerDetailViewController {

    private static final Logger logger = LoggerFactory.getLogger(CustomerDetailViewController.class);

    @FXML
    private TextField idTextField;

    @FXML
    private TextField emailTextField;

    @FXML
    private TextField firstNameTextField;

    @FXML
    private TextField lastNameTextField;

    @FXML
    private TextField cityTextField;

    @FXML
    private TextField streetNameTextField;

    @FXML
    private TextField streetNumberTextField;

    @FXML
    private TextField addressTypeTextField;

    // used to reference the stage and to get passed data through it
    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        idTextField.setEditable(false);
        emailTextField.setEditable(false);
        firstNameTextField.setEditable(false);
        lastNameTextField.setEditable(false);
        cityTextField.setEditable(false);
        streetNameTextField.setEditable(false);
        streetNumberTextField.setEditable(false);
        addressTypeTextField.setEditable(false);

        loadCustomersData();

        logger.info("PersonsDetailViewController initialized");
    }

    private void loadCustomersData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof CustomerDetailView) {
            CustomerDetailView customerBasicView = (CustomerDetailView) stage.getUserData();
            idTextField.setText(String.valueOf(customerBasicView.getId()));
            emailTextField.setText(customerBasicView.getEmail());
            firstNameTextField.setText(customerBasicView.getFirstName());
            lastNameTextField.setText(customerBasicView.getLastName());
            cityTextField.setText(customerBasicView.getCity());
            streetNameTextField.setText(customerBasicView.getStreetName());
            streetNumberTextField.setText(customerBasicView.getStreetNumber());
            addressTypeTextField.setText(customerBasicView.getAddressType());
        }
    }

}
