package org.but.feec.javafx.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import org.but.feec.javafx.api.CustomerCreateView;
import org.but.feec.javafx.data.CustomerRepository;
import org.but.feec.javafx.services.CustomerService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class CustomerCreateController {

    private static final Logger logger = LoggerFactory.getLogger(CustomerCreateController.class);

    @FXML
    public Button newCustomerCreateCustomer;
    @FXML
    private TextField newCustomerEmail;

    @FXML
    private TextField newCustomerFirstName;

    @FXML
    private TextField newCustomerLastName;

    @FXML
    private TextField newCustomerPwd;

    @FXML
    private TextField newCustomerGender;

    private CustomerService customerService;
    private CustomerRepository customerRepository;
    private ValidationSupport validation;

    @FXML
    public void initialize() {
        customerRepository = new CustomerRepository();
        customerService = new CustomerService(customerRepository);

        validation = new ValidationSupport();
        validation.registerValidator(newCustomerEmail, Validator.createEmptyValidator("The email must not be empty."));
        validation.registerValidator(newCustomerFirstName, Validator.createEmptyValidator("The first name must not be empty."));
        validation.registerValidator(newCustomerLastName, Validator.createEmptyValidator("The last name must not be empty."));
        validation.registerValidator(newCustomerPwd, Validator.createEmptyValidator("The password must not be empty."));
        validation.registerValidator(newCustomerGender, Validator.createEmptyValidator("The gender must not be empty."));

        newCustomerCreateCustomer.disableProperty().bind(validation.invalidProperty());

        logger.info("PersonCreateController initialized");
    }

    @FXML
    void handleCreateNewCustomer(ActionEvent event) {
        String email = newCustomerEmail.getText();
        String firstName = newCustomerFirstName.getText();
        String lastName = newCustomerLastName.getText();
        String password = newCustomerPwd.getText();
        String gender = newCustomerGender.getText();

        CustomerCreateView customerCreateView = new CustomerCreateView();
        customerCreateView.setPwd(password.toCharArray());
        customerCreateView.setEmail(email);
        customerCreateView.setFirstName(firstName);
        customerCreateView.setLastName(lastName);
        customerCreateView.setGender(gender.toCharArray());


        customerService.createCustomer(customerCreateView);

        customerCreatedConfirmationDialog();
    }

    private void customerCreatedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Customer Created Confirmation");
        alert.setHeaderText("Your customer was successfully created.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }

}
