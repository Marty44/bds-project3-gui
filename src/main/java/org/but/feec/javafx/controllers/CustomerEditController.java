package org.but.feec.javafx.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.javafx.api.CustomerBasicView;
import org.but.feec.javafx.api.CustomerEditView;
import org.but.feec.javafx.data.CustomerRepository;
import org.but.feec.javafx.services.CustomerService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;


public class CustomerEditController {

    private static final Logger logger = LoggerFactory.getLogger(CustomerEditController.class);

    @FXML
    public Button editCustomerButton;
    @FXML
    public TextField idTextField;
    @FXML
    private TextField emailTextField;
    @FXML
    private TextField firstNameTextField;
    @FXML
    private TextField lastNameTextField;
    @FXML
    private TextField cityTextField;


    private CustomerService customerService;
    private CustomerRepository customerRepository;
    private ValidationSupport validation;

    // used to reference the stage and to get passed data through it
    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        customerRepository = new CustomerRepository();
        customerService = new CustomerService(customerRepository);

        validation = new ValidationSupport();
        validation.registerValidator(idTextField, Validator.createEmptyValidator("The id must not be empty."));
        idTextField.setEditable(false);
        validation.registerValidator(emailTextField, Validator.createEmptyValidator("The email must not be empty."));
        validation.registerValidator(firstNameTextField, Validator.createEmptyValidator("The first name must not be empty."));
        validation.registerValidator(lastNameTextField, Validator.createEmptyValidator("The last name must not be empty."));
        validation.registerValidator(cityTextField, Validator.createEmptyValidator("The last name must not be empty."));

        editCustomerButton.disableProperty().bind(validation.invalidProperty());

        loadCustomersData();

        logger.info("CustomerEditController initialized");
    }


    private void loadCustomersData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof CustomerBasicView) {
            CustomerBasicView customerBasicView = (CustomerBasicView) stage.getUserData();
            idTextField.setText(String.valueOf(customerBasicView.getId()));
            emailTextField.setText(customerBasicView.getEmail());
            firstNameTextField.setText(customerBasicView.getFirstName());
            lastNameTextField.setText(customerBasicView.getLastName());
            cityTextField.setText(customerBasicView.getCity());
        }
    }

    @FXML
    public void handleEditCustomerButton(ActionEvent event) {

        Long id = Long.valueOf(idTextField.getText());
        String email = emailTextField.getText();
        String firstName = firstNameTextField.getText();
        String lastName = lastNameTextField.getText();


        CustomerEditView customerEditView = new CustomerEditView();
        customerEditView.setId(id);
        customerEditView.setEmail(email);
        customerEditView.setFirstName(firstName);
        customerEditView.setLastName(lastName);


        customerService.editCustomer(customerEditView);

        personEditedConfirmationDialog();
    }

    private void personEditedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Customer Edited Confirmation");
        alert.setHeaderText("Your customer was successfully edited.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }

}
