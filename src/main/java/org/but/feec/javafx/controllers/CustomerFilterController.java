package org.but.feec.javafx.controllers;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.but.feec.javafx.api.CustomerFilterView;
import org.but.feec.javafx.data.CustomerRepository;
import org.but.feec.javafx.services.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CustomerFilterController {
    private static final Logger logger = LoggerFactory.getLogger(CustomerFilterController.class);

    @FXML
    private TableColumn<CustomerFilterView, Long> CustomerId;
    @FXML
    private TableColumn<CustomerFilterView, String> Email;
    @FXML
    private TableColumn<CustomerFilterView, String> FirstName;
    @FXML
    private TableColumn<CustomerFilterView, String> LastName;
    @FXML
    private TableColumn<CustomerFilterView, String> City;

    @FXML
    private TableView<CustomerFilterView> systemFilterCustomersTableView;

    private CustomerService customerService;
    private CustomerRepository customerRepository;

    public Stage stage;

    public CustomerFilterController() {
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void initialize() {
        customerRepository = new CustomerRepository();
        customerService = new CustomerService(customerRepository);
//        GlyphsDude.setIcon(exitMenuItem, FontAwesomeIcon.CLOSE, "1em");

        CustomerId.setCellValueFactory(new PropertyValueFactory<CustomerFilterView, Long>("id"));
        City.setCellValueFactory(new PropertyValueFactory<CustomerFilterView, String>("city"));
        Email.setCellValueFactory(new PropertyValueFactory<CustomerFilterView, String>("email"));
        FirstName.setCellValueFactory(new PropertyValueFactory<CustomerFilterView, String>("firstName"));
        LastName.setCellValueFactory(new PropertyValueFactory<CustomerFilterView, String>("lastName"));


        ObservableList<CustomerFilterView> observablePersonsList = initializeCustomersData();
        systemFilterCustomersTableView.setItems(observablePersonsList);




        logger.info("CustomerFilter Controller initialized");


    }
    private ObservableList<CustomerFilterView> initializeCustomersData() {

        String text = (String) stage.getUserData();
        List<CustomerFilterView> persons = customerService.getCustomerFilterView(text);
        return FXCollections.observableArrayList(persons);
    }


}
