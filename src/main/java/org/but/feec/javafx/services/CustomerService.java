package org.but.feec.javafx.services;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.but.feec.javafx.api.*;
import org.but.feec.javafx.data.CustomerRepository;

import java.util.List;


public class CustomerService {

    private CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public CustomerDetailView getCustomerDetailView(Long id) {
        return customerRepository.findCustomerDetailedView(id);
    }

    public List<CustomerBasicView> getCustomersBasicView() {
        return customerRepository.getCustomersBasicView();
    }
    public List<CustomerFilterView> getCustomerFilterView(String text) {
        return customerRepository.getCustomerFilterView(text);
    }
    public List<SqlInjectView> getSqlInjectView(String input){
        return customerRepository.getSqlInjectView(input);
    }

    public void createCustomer(CustomerCreateView customerCreateView) {

        char[] originalPassword = customerCreateView.getPwd();
        char[] hashedPassword = hashPassword(originalPassword);
        customerCreateView.setPwd(hashedPassword);

        customerRepository.createCustomer(customerCreateView);
    }

    public void editCustomer(CustomerEditView customerEditView) {
        customerRepository.editCustomer(customerEditView);
    }


    private char[] hashPassword(char[] password) {
        return BCrypt.withDefaults().hashToChar(12, password);
    }


}