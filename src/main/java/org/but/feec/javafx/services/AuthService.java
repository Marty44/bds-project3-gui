package org.but.feec.javafx.services;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.but.feec.javafx.api.CustomerAuthView;
import org.but.feec.javafx.data.CustomerRepository;
import org.but.feec.javafx.exceptions.ResourceNotFoundException;

public class AuthService {

    private CustomerRepository customerRepository;

    public AuthService(CustomerRepository personRepository) {
        this.customerRepository = personRepository;
    }

    private CustomerAuthView findCustomerByFirstName(String first_name) {
        return customerRepository.findCustomerByFirstName(first_name);
    }


    public boolean authenticate(String username, String password) {
        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            return false;
        }

        CustomerAuthView customerAuthView = findCustomerByFirstName(username);
        if (customerAuthView == null) {
            throw new ResourceNotFoundException("Provided username is not found.");
        }


        BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), customerAuthView.getPassword());
        return result.verified;
    }




}
