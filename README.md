# bds-project3-gui
Implementation of GUI using JavaFX on databes we crated earlier this semester.
Project is based on code which created our tacher.
I can't imagine making this project without it. 
## Project Name
BDS JavaFx Project-3

## Description
To build and run this project, you need to use these commands in project root directory:
* build:
    ```
    $ mvn clean install
    ```
* run:
    ```
    $ java -jar target/bds-javafx-training-1.0.0. jar
    ```
### Features
* It's using authentication method. You must login into database with login name and password. 
* At first we can see basic view of the database. When we perform right click on some db entity we can edit, delete that entity or we can se detailed view of the entity.
* We can filter entities based on the first name eg. it shows db entities with the same name.
* We can perform some kind of sql injection on random table which was created for this purpose.

## License
Licensed under Apache License 2.0

